#pragma once

#include <cstddef>
#include <compare>

struct vec2 {
  vec2() = default;
  vec2(float x, float y)
  : x(x)
  , y(y)
  {}
  auto operator<=>(const vec2&) const = default;
  float len2() const {
    return x*x + y*y;
  }
  float len() const {
    return sqrt(len2());
  }
  float dot(const vec2& v) const {
    return x * v.x + y * v.y;
  }
  float cross(const vec2& v) const {
    return x * v.y - y * v.x;
  }
  vec2 operator-(const vec2& v) const {
    return { x - v.x, y - v.y };
  }
  vec2 operator+(const vec2& v) const {
    return { x + v.x, y + v.y };
  }
  friend vec2 operator*(float s, const vec2& v) {
    return {v.x * s, v.y * s};
  }
  vec2 operator*(float s) const {
    return {x * s, y * s};
  }
  vec2 project(const vec2& e1, const vec2& e2) const {
    return { (*this).dot(e1), (*this).dot(e2) };
  }
  float x = 0.0f, y = 0.0f;
  const float& operator[](size_t index) const {
    return (index == 0) ? x : y;
  }
  float& operator[](size_t index) {
    return (index == 0) ? x : y;
  }
  vec2& operator+=(const vec2& v) {
    x += v.x;
    y += v.y;
    return *this;
  }
};


