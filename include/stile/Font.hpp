#pragma once

#include <stile/vec2.hpp>
#include <vector>
#include <filesystem>

namespace Stile {

struct Mesh
{
  struct face { 
    face(int v1, int v2, int v3) 
    : v1(v1)
    , v2(v2)
    , v3(v3)
    {}
    int v1; 
    int v2; 
    int v3; 
  };
  std::vector<vec2> vertices;
  std::vector<face> faces;
  void AddMesh(Mesh&& mesh, uint16_t paletteEntry); // somehow specify gradient
};

struct Font {
  struct Metrics {
    float ascender;
    float descender;
    float lineGap;
  };
  virtual ~Font() = default;
  virtual Metrics GetMetrics() = 0;
  virtual float GetGlyphAdvanceWidth(char32_t ch) = 0;
  virtual Mesh GetGlyph(char32_t ch) = 0;
};

std::shared_ptr<Font> LoadFont(std::filesystem::path path);

}

// wrap type: 0: t = clamp(0.0, t, 1.0); 1: t = t % 1.0; 2: t = 1 - std::abs(-(t % 2) + 1)

// linear gradient, defined with rotation in radians to align gradient with X axis, and distance to scale, plus wrap type
// transform from (x,y) to (t on gradient)
// sweep gradient, defined with x/y center point, and start/end angles both defaulted to zero
// transform from (x,y) to (t on gradient), or discard
// radial gradient, with wrap type
// transform from (x,y) to (t on gradient), or discard

// cut line 1: rotation, offset
// cut line 2: rotation, offset

/*
0. degenerate (c0=c1, r0==r1==0) -> discard all
1. circle fully contained in other circle delta(c0,c1)+min(r0,r1) < max(r0,r1) -> surface covering
2. circle edge-contained in other circle delta(c1,c2)+min(r0,r1) == max(r0,r1) -> half surface covering (one cut line)
3. circle not contained in other circle
  3.1 same size: r0==r1 (two parallel cut lines)
  3.2 different size: two cut lines

1, 2, 3.2: focal point F of gradient at c1 + (|c1-c0| + min(r0,r1))/(r0/r1)
*/

